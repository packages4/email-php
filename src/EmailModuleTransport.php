<?php

namespace Quince\Platform\Email;

use GuzzleHttp\ClientInterface;
use Illuminate\Mail\Transport\Transport;
use Swift_Attachment;
use Swift_Image;
use Swift_Mime_SimpleMessage;
use Swift_MimePart;

class EmailModuleTransport extends Transport
{
    const MAXIMUM_FILE_SIZE = 26214400;

    /**
     * Guzzle client instance.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $client;

    /**
     * THe Platform Email API end-point.
     *
     * @var string
     */
    protected $url;

    /**
     * Create a new Platform Email transport instance.
     *
     * @param \GuzzleHttp\ClientInterface $client
     * @param string                      $domain
     */
    public function __construct(ClientInterface $client, $domain)
    {
        $this->client = $client;
        $this->setUrl($domain);
    }

    public function send(Swift_Mime_SimpleMessage $message, &$failedRecipients = null)
    {
        if (method_exists($this, 'beforeSendPerformed')) {
            $this->beforeSendPerformed($message);
        }

        $this->client->post($this->url, ['json' => $this->payload($message)]);

        if (method_exists($this, 'sendPerformed')) {
            $this->sendPerformed($message);
        }

        if (method_exists($this, 'numberOfRecipients')) {
            return $this->numberOfRecipients($message);
        }

        return true;
    }

    /**
     * Get the HTTP payload for sending the Platform Email message.
     *
     * @param \Swift_Mime_SimpleMessage $message
     * @param string                    $to
     *
     * @return array
     */
    protected function payload(Swift_Mime_SimpleMessage $message)
    {
        $contents = $this->getContents($message);

        return [
            'to' => $this->parseAddressesFromMessage($message, 'To'),
            'cc' => $this->parseAddressesFromMessage($message, 'Cc'),
            'bcc' => $this->parseAddressesFromMessage($message, 'Bcc'),
            'from' => $this->parseAddressesFromMessage($message, 'From', true),
            'replyto' => $this->parseAddressesFromMessage($message, 'ReplyTo', true),
            'body_text' => isset($contents['text/plain']) ? $contents['text/plain'] : '',
            'body_html' => isset($contents['text/html']) ? $contents['text/html'] : '',
            'subject' => $message->getSubject(),
            'attachments' => $this->getAttachments($message),
        ];
    }

    protected function parseAddressesFromMessage($message, $type, $returnSingleAddress = false)
    {
        $addresses = [];
        foreach ((array) $message->{"get{$type}"}() as $address => $name) {
            $addresses[] = ['email' => $address] + ($name ? ['name' => $name] : []);
        }

        if ($returnSingleAddress) {
            return array_shift($addresses);
        }

        return $addresses;
    }

    /**
     * Get contents.
     *
     * @param Swift_Mime_SimpleMessage $message
     *
     * @return array
     */
    private function getContents(Swift_Mime_SimpleMessage $message)
    {
        $contentType = $message->getContentType();
        switch ($contentType) {
            case 'text/plain':
                return ['text/plain' => $message->getBody()];
            case 'text/html':
                return ['text/html' => $message->getBody()];
        }
        // Following RFC 1341, text/html after text/plain in multipart
        $content = [];
        foreach ($message->getChildren() as $child) {
            if ($child instanceof Swift_MimePart && 'text/plain' == $child->getContentType()) {
                $content['text/plain'] = $child->getBody();
            }
        }

        $content['text/html'] = $message->getBody();

        return $content;
    }

    /**
     * Get attachments from Swift Message.
     *
     * @param Swift_Mime_SimpleMessage $message
     *
     * @return array
     */
    private function getAttachments(Swift_Mime_SimpleMessage $message)
    {
        $allAttachmentParams = [];
        foreach ($message->getChildren() as $attachment) {
            if ((!$attachment instanceof Swift_Attachment && !$attachment instanceof Swift_Image)
                || !strlen($attachment->getBody()) > self::MAXIMUM_FILE_SIZE
            ) {
                continue;
            }

            $attachmentParams = [
                'content' => base64_encode($attachment->getBody()),
                'filename' => $attachment->getFilename(),
                'mime_type' => $attachment->getContentType(),
            ];

            if ($attachment instanceof Swift_Image) {
                $attachmentParams['disposition'] = 'inline';
                $attachmentParams['content_id'] = $attachment->getId();
            }

            $allAttachmentParams[] = $attachmentParams;
        }

        return $allAttachmentParams;
    }

    /**
     * Set the url being used by the transport.
     *
     * @param string $domain
     */
    public function setUrl($domain)
    {
        $this->url = $domain . '/email/v0/messages';
    }
}
