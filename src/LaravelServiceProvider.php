<?php

namespace Quince\Platform\Email;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\MailServiceProvider;
use Swift_Mailer;

class LaravelServiceProvider extends MailServiceProvider
{
    /**
     * Register the Swift Mailer instance.
     */
    public function registerSwiftMailer()
    {
        if ('quinceplatform' == $this->app['config']['mail.driver']) {
            $this->registerEmailModuleSwiftMailer();
        } else {
            parent::registerSwiftMailer();
        }
    }

    /**
     * Initialise Swift Mailer with a Email Module transport instance.
     */
    protected function registerEmailModuleSwiftMailer()
    {
        $this->app->singleton('swift.mailer', function ($app) {
            return new Swift_Mailer(new EmailModuleTransport(
                new HttpClient(['connect_timeout' => 60]),
                env('PLATFORM_EMAIL_URL', 'http://localhost')
            ));
        });
    }
}
