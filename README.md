# Platform Email module PHP SDK

> Contains Laravel Mail driver to send email through the Platform Email module

## How to install
To use this package in you project
```bash
composer require quince/platform-email-sdk
```

The mail driver require the Guzzle HTTP library, which may be installed via the Composer package manager
```bash
composer require guzzlehttp/guzzle
```

Set environment variable for Platform API domain
```
PLATFORM_EMAIL_URL=https://api.platform.quince.global
```

### Using with Laravel/Lumen Project

Register the package service provider class to your app
```
// For laravel add to config/app.php
Quince\Platform\Email\LaravelServiceProvider::class

// For Lumen add to bootstrap/app.php
$app->register(Quince\Platform\Email\LaravelServiceProvider::class);
```

Change Laravel Mail driver in your enviroment variables
```
MAIL_DRIVER=quinceplatform
```

## How to setup development
You can clone this repo inside a Laravel app for easlier development/testing.
1. Create Laravel app `composer create-project --prefer-dist laravel/laravel emailtest`
2. Clone package repo into `packages/quince/platform-email-sdk` folder
3. Add the following to Laravel `composer.json` file (to make package visible):
```
    "autoload": {
        "psr-4": {
            "Quince\\Platform\\Email\\": "packages/platform-email-sdk/src"
        }
    },
```
4. Run `composer dump-autoload`
5. Follow "installation" instructions above to use the package.

Now you can use the package you are developing in an app, without having to publish before each test.

## How to lint
```bash
# To lint and fix your php source files
composer run -- lint-fix
```

## How to test

There are currently no tests for this repo
